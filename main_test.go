package main

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"
)

// Example TestDiscoverOpenAPIsHandler Test which makes sure 400 is returned for empty request bodies.
func TestDiscoverOpenAPIsHandler(t *testing.T) {
	r := httptest.NewRequest(http.MethodPost, "/", bytes.NewReader([]byte{}))
	w := httptest.NewRecorder()
	DiscoverHandler(w, r)
	if w.Result().StatusCode != 400 {
		t.Error("expect StatusCode 400 bad request if body is empty")
	}
}

// Example DoDiscoverOpenAPIs Test which does not actually do anything.
// func TestDoDiscoverOpenAPIs(t *testing.T) {
// 	var err error
// 	initialData := &common.Data{
// 		IndustryID: "",
// 		DomainID: "",
// 		SchemaID: "",
// 		FirestoreID: "TestDoDiscoverOpenAPIs",
// 	}
// 	initialData.Init(nil)
// 	err = allDB.Create(initialData)
// 	if err != nil {
// 		t.Fatal(err.Error())
// 	}
// 	httpsCallRequest := actions.HTTPSCallRequest{
// 		CurrentData:    map[string]interface{}{
// 			common.PolyappIndustryID: "",
// 			common.PolyappDomainID: "",
// 			common.PolyappSchemaID: "",
// 		},
// 		ReferencedData: map[string]map[string]interface{}{},
// 		Request:        &common.POSTRequest{
// 			DataID: "TestDoDiscoverOpenAPIs",
// 		},
// 		Response:       &common.POSTResponse{},
// 		BotStaticData:  map[string]string{},
// 	}
// 	httpsCallResponse := actions.HTTPSCallResponse{
// 		Request:     &common.POSTRequest{},
// 		Response:    &common.POSTResponse{},
// 		CreateDatas: []common.Data{},
// 		UpdateDatas: []common.Data{},
// 		DeleteDatas: []string{},
// 	}
// 	err = DoDiscoverOpenAPIs(&httpsCallRequest, &httpsCallResponse)
// 	if err != nil {
// 		t.Error(err.Error())
// 	}
// 	err = allDB.DeleteData("TestDoDiscoverOpenAPIs")
// 	if err != nil {
// 		t.Fatal(err.Error())
// 	}
// 	if httpsCallResponse.Response.DataUpdates != nil && len(httpsCallResponse.Response.DataUpdates) > 0 {
// 		for k, v := range httpsCallResponse.Response.DataUpdates {
// 			if k == "" {
// 				t.Errorf("httpsCallResponse.Response.DataUpdates[%v] key was empty - this will be invalid on the client", k)
// 			}
// 			for fieldKey := range v {
// 				if fieldKey == "" {
// 					t.Errorf("httpsCallResponse.Response.DataUpdates[%v][%v] fieldKey was invalid because it was empty", k, fieldKey)
// 				}
// 				if !strings.HasPrefix(fieldKey, "polyapp") {
// 					ind, dom, sch, field := common.SplitField(fieldKey)
// 					if ind == "" || dom == "" || sch == "" || field == "" {
// 						t.Errorf("httpsCallResponse.Response.DataUpdates[%v][%v] fieldKey was invalid because ind / dom / sch / field was empty - this will be invalid on the client", k, fieldKey)
// 					}
// 				}
// 			}
// 		}
// 	}
// }
