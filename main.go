package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/actions"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8081"
	}
	http.HandleFunc("/discover", DiscoverHandler)
	http.HandleFunc("/play", PlayHandler)
	http.HandleFunc("/details", DetailsHandler)

	// TLSPath is used for local development on SSL.
	TLSPath, TLSPathExists := os.LookupEnv("TLSPATH")
	if !TLSPathExists {
		log.Fatal(http.ListenAndServe(":"+port, nil))
	} else {
		cert := TLSPath + string(os.PathSeparator) + "server.crt"
		key := TLSPath + string(os.PathSeparator) + "server.key"
		log.Fatal(http.ListenAndServeTLS(":"+port, cert, key, nil))
	}
}

type Discover struct {
	Done           bool      `suffix:"Done"`
	SearchAPIs     string    `suffix:"Search APIs"`
}

// DiscoverHandler translates a request into a format usable by func Discover
// It should be triggered via a POST request since a body is required, but any HTTP verb is accepted.
func DiscoverHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("ioutil.ReadAll: %v", err.Error()), 400)
		return
	}
	var HTTPSCallRequest actions.HTTPSCallRequest
	HTTPSCallResponse := actions.HTTPSCallResponse{
		Request:     &common.POSTRequest{},
		Response:    &common.POSTResponse{},
		CreateDatas: []common.Data{},
		UpdateDatas: []common.Data{},
		DeleteDatas: []string{},
	}
	err = json.Unmarshal(body, &HTTPSCallRequest)
	if err != nil {
		http.Error(w, fmt.Sprintf("json.Unmarshal: %v", err.Error()), 400)
		return
	}
	err = DoDiscover(&HTTPSCallRequest, &HTTPSCallResponse)
	if err != nil {
		http.Error(w, fmt.Sprintf("DoDiscover: %v", err.Error()), 500)
		return
	}
	responseBytes, err := json.Marshal(HTTPSCallResponse)
	if err != nil {
		http.Error(w, fmt.Sprintf("json.Marshal: %v", err.Error()), 500)
		return
	}
	_, err = w.Write(responseBytes)
	if err != nil {
		http.Error(w, fmt.Sprintf("w.Write: %v", err.Error()), 500)
		return
	}
}

// DoDiscover Discover APIs by searching our list of known Open APIs
func DoDiscover(request *actions.HTTPSCallRequest, response *actions.HTTPSCallResponse) error {
	var err error
	data := common.Data{}
	err = data.Init(request.CurrentData)
	if err != nil {
		return fmt.Errorf("data.Init: %w", err)
	}
	data.FirestoreID = request.Request.DataID
	Discover := Discover{}
	err = common.DataIntoStructure(&data, &Discover)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}

	// Save any updates which happened to Data into the database
	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &Discover, &data)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(&data)
	if err != nil {
		return fmt.Errorf("allDB.updateData: %w", err)
	}

	return nil
}

type Play struct {
	Done           bool      `suffix:"Done"`
	SearchAPIs     string    `suffix:"Search APIs"`
}

// PlayHandler translates a request into a format usable by func Play
// It should be triggered via a POST request since a body is required, but any HTTP verb is accepted.
func PlayHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("ioutil.ReadAll: %v", err.Error()), 400)
		return
	}
	var HTTPSCallRequest actions.HTTPSCallRequest
	HTTPSCallResponse := actions.HTTPSCallResponse{
		Request:     &common.POSTRequest{},
		Response:    &common.POSTResponse{},
		CreateDatas: []common.Data{},
		UpdateDatas: []common.Data{},
		DeleteDatas: []string{},
	}
	err = json.Unmarshal(body, &HTTPSCallRequest)
	if err != nil {
		http.Error(w, fmt.Sprintf("json.Unmarshal: %v", err.Error()), 400)
		return
	}
	err = DoPlay(&HTTPSCallRequest, &HTTPSCallResponse)
	if err != nil {
		http.Error(w, fmt.Sprintf("DoPlay: %v", err.Error()), 500)
		return
	}
	responseBytes, err := json.Marshal(HTTPSCallResponse)
	if err != nil {
		http.Error(w, fmt.Sprintf("json.Marshal: %v", err.Error()), 500)
		return
	}
	_, err = w.Write(responseBytes)
	if err != nil {
		http.Error(w, fmt.Sprintf("w.Write: %v", err.Error()), 500)
		return
	}
}

// DoPlay lets you play with an API which is following the OpenAPI standard.
func DoPlay(request *actions.HTTPSCallRequest, response *actions.HTTPSCallResponse) error {
	var err error
	data := common.Data{}
	err = data.Init(request.CurrentData)
	if err != nil {
		return fmt.Errorf("data.Init: %w", err)
	}
	data.FirestoreID = request.Request.DataID
	Play := Play{}
	err = common.DataIntoStructure(&data, &Play)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}

	// Save any updates which happened to Data into the database
	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &Play, &data)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(&data)
	if err != nil {
		return fmt.Errorf("allDB.updateData: %w", err)
	}

	return nil
}

type Details struct {
	Done           bool      `suffix:"Done"`
	SearchAPIs     string    `suffix:"Search APIs"`
}

// DetailsHandler translates a request into a format usable by func Details
// It should be triggered via a POST request since a body is required, but any HTTP verb is accepted.
func DetailsHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("ioutil.ReadAll: %v", err.Error()), 400)
		return
	}
	var HTTPSCallRequest actions.HTTPSCallRequest
	HTTPSCallResponse := actions.HTTPSCallResponse{
		Request:     &common.POSTRequest{},
		Response:    &common.POSTResponse{},
		CreateDatas: []common.Data{},
		UpdateDatas: []common.Data{},
		DeleteDatas: []string{},
	}
	err = json.Unmarshal(body, &HTTPSCallRequest)
	if err != nil {
		http.Error(w, fmt.Sprintf("json.Unmarshal: %v", err.Error()), 400)
		return
	}
	err = DoDetails(&HTTPSCallRequest, &HTTPSCallResponse)
	if err != nil {
		http.Error(w, fmt.Sprintf("DoDetails: %v", err.Error()), 500)
		return
	}
	responseBytes, err := json.Marshal(HTTPSCallResponse)
	if err != nil {
		http.Error(w, fmt.Sprintf("json.Marshal: %v", err.Error()), 500)
		return
	}
	_, err = w.Write(responseBytes)
	if err != nil {
		http.Error(w, fmt.Sprintf("w.Write: %v", err.Error()), 500)
		return
	}
}

// DoDetails lets you view a client SDK for a variety of languages.
func DoDetails(request *actions.HTTPSCallRequest, response *actions.HTTPSCallResponse) error {
	var err error
	data := common.Data{}
	err = data.Init(request.CurrentData)
	if err != nil {
		return fmt.Errorf("data.Init: %w", err)
	}
	data.FirestoreID = request.Request.DataID
	Details := Details{}
	err = common.DataIntoStructure(&data, &Details)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}

	// Save any updates which happened to Data into the database
	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &Details, &data)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(&data)
	if err != nil {
		return fmt.Errorf("allDB.updateData: %w", err)
	}

	return nil
}
